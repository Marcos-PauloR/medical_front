import React, { ReactElement, useEffect, useRef, useState } from 'react';
import 'primeicons/primeicons.css';


interface ProfileSettingsProps {
  onClickOutside?: () => void;
  visible?: boolean; 
}

export default function ProfileSettings(props: ProfileSettingsProps){
  const ref = useRef<HTMLDivElement>(null);

  useEffect(() => {
    
    const handleClickOutside = (event: MouseEvent) => {
      if (ref.current && !ref.current.contains(event.target as Node) && ((event.target as HTMLElement).className!=='teste' )) {
        props.onClickOutside && props.onClickOutside();
      }
    };


    document.addEventListener('click', handleClickOutside, true);
    return () => {
      document.removeEventListener('click', handleClickOutside, true);
    };
  }, [props, props.onClickOutside]);

  
    return (
    <div ref={ref} style={{border:'0', background:'#F6F8FC', width:'100%', height:'100%', visibility:props.visible?'visible':'hidden' ,padding:'8px 8px' ,borderRadius:'6px', boxShadow:'0 1px 6px 0 rgb(32 33 36 / 28%)'}} >
      <div style={{height:'88%', width:'100%', background:'#FFF'}}>

      </div>
      <button style={{border:'0px', width:'100%', backgroundColor :'#F6F8FC', display:'flex'  ,alignItems:'center', height:'10%', marginTop:'2%'}} onClick={()=>{console.log('teste foco de dentro do butao')}}>
        <div style={{width:'20%'}}>

        <i className="pi pi-sign-out"></i>
        </div>
        <div style={{width:'60%', fontFamily:'roboto', fontSize:'14px', fontWeight:'500' }}> 

         Sair do Sistema
        </div>
      </button>
    </div>
  )
}
