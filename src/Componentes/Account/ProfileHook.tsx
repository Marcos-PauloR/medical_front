import { useEffect, useState } from 'react';

function useLocalStorageChange(key: string): string {
  const [isLocalStorage, setIsLocalStorage] = useState('');

  useEffect(() => {
    const onStorageChange = (event: StorageEvent): string => {
      if (event.key === key) {
            setIsLocalStorage( JSON.parse(localStorage.getItem(key) || ""))
            return isLocalStorage
      }
      else{
        return ""
      }
    };

    window.addEventListener('storage', onStorageChange);

    return () => {
      window.removeEventListener('storage', onStorageChange);
    };
  }, [key]);

  return isLocalStorage;
}

export default useLocalStorageChange;