import React, { useEffect, useRef, useState } from 'react';

export interface ProfileProps {
 onClick?:()=>void;
 className: string;
}
export default function Profile(props: ProfileProps){

  const [usrName, setUsrName] = useState('GABRIELS - Gabriel Santos');
  const [departamento, setDepartamento] = useState('Desenvolvedor de Software');
  const ref = useRef<HTMLButtonElement>(null)

  useEffect(() => {
    
    const handleClick = (event: MouseEvent) => {
        if (ref.current && ref.current.contains(event.target as Node)){
            props.onClick && props.onClick();
        } 
    };


    const handleStorageEvent = (event: StorageEvent)=>{
        console.log(event.key)
        if(localStorage.length>0 && localStorage.getItem('usuario')){
        const storage = JSON.parse(localStorage.getItem("usuario") || "") 
        setUsrName(storage.usuario+' - '+storage.nome)
        setDepartamento(storage.departamento)

      }
    }

    document.addEventListener('click', handleClick, true);
    window.addEventListener('storage', handleStorageEvent);
    return () => {
      document.removeEventListener('click', handleClick, true);
      window.removeEventListener('storage', handleStorageEvent);
    };
  }, [props, props.onClick]); 


  return (
    <button className={props.className} ref={ref} style={{ borderRadius:'6px', backgroundColor:'#F6F6F6', border:'0px', width:'220px', height:'28px', display:'flex',alignItems:'center', justifyContent:'space-between'}}   >
                                                          
        <div className={props.className} style={{display:'flex',alignItems:'center', justifyContent:'space-between'}}>

            <div className={props.className} style={{background:'#7A53B5', width:'22px', height:'22px', borderRadius:'50%', display:'flex', alignItems:'center', justifyContent:'center', color:'#FFF'}}>
                <span>
                G
                </span>
                
                </div>

                <div className={props.className} style={{display:'flex', flexDirection:'column', justifyContent:'left', marginLeft:'3px'}}>
                
                    <div className={props.className} style={{fontSize:'0.688rem', fontWeight:'500', lineHeight:'0.813rem',}}>
                        {usrName}
                    </div>
                    <div className={props.className} style={{fontSize:'0.563rem', fontWeight:'300', lineHeight:'0.625rem', display:'flex' ,justifyContent:'left'}}>
                        {departamento}
                    </div>
                
                </div>  
        
        </div>  

            <div className={props.className} style={{color:'#CECECE'}}>
              <img  alt="" src="/assets/icons/Group 130.svg" style={{height:'11px', width:'11px'}} />
            </div>
    </button>
  )
}