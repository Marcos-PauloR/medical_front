import React, { useState, useEffect, useRef }  from "react";
import "./Header.css";
import Perfil from "./Perfil/Profile";
import ReactDOM from "react-dom";
import Profile from "./Account/ProfileButton";


interface HeaderProps{
  atualizaUsuario?: boolean
}


export default function Header(props:HeaderProps) {
  const [verPerfil, setVerPerfil] = useState(false);

  return (
    <header
      style={{
        backgroundColor: "#FFFFFF",
        height: "2.875rem",
        maxHeight: "2.875rem",
        position: "sticky",
        display: "flex",
        top: "0",
        padding: "2px 9px",
        justifyContent: "space-between",
      }}
    >
      <div style={{ width: "5,625rem", height: "2,625rem" }}>
        <img
          style={{ width: "5,625rem", height: "2,625rem" }}
          alt=""
          src="/assets/CBCO.svg"
        />
      </div>

      <div style={{ display: "flex", alignItems: "center" }}>
        <button className="btnLink">Paciente</button>
        <button className="btnLink">Agendas</button>
        <button className="btnLink">Finanças</button>
        <button className="btnLink">Médicos</button>
        <button className="btnLink">Serviços</button>
      </div>

      <div style={{ display: "flex", alignItems: "center", }}>
        <div style={{ display: "flex", alignItems: "center", justifyContent:"space-between", marginRight:'12px' }}>
        
          <button style={{height:'15px', width:'15px', border:'0px', backgroundColor:'#FFFFFF', color:'#6B6D71', display:'flex', justifyContent:'center', alignItems:'center', padding:'2px', margin:'0 1px'}}>
            <img alt="" src="/assets/icons/SearchOutlined.svg" style={{height:'11px', width:'11px'}} />
          </button>
          <button style={{height:'15px', width:'15px', border:'0px', backgroundColor:'#FFFFFF', color:'#6B6D71', display:'flex', justifyContent:'center', alignItems:'center'}}>
            <img alt="" src="/assets/icons/AppstoreOutlined.svg" style={{height:'11px', width:'11px'}} />
          </button>
          <button style={{height:'15px', width:'15px', border:'0px', backgroundColor:'#FFFFFF', color:'#6B6D71', display:'flex', justifyContent:'center', alignItems:'center'}}>
            <img alt="" src="/assets/icons/ExceptionOutlined.svg" style={{height:'11px', width:'11px'}} />
          </button>
          <button style={{height:'15px', width:'15px', border:'0px', backgroundColor:'#FFFFFF', color:'#6B6D71', display:'flex', justifyContent:'center', alignItems:'center'}}>
            <img alt="" src="/assets/icons/SettingOutlined.svg" style={{height:'11px', width:'11px'}} />
          </button>

          <div style={{height:'28px', border:'0.5px solid #9D9D9D', margin:'0 0.375rem'}}></div>

          

          <Profile className="teste" onClick={ ()=>{ setVerPerfil(!verPerfil)}} />

        </div>


        <button style={{height:'18px', width:'18px', display:'flex',alignItems:'center', justifyContent:'center', border:'0', background:'none'}}>
                    <img alt="" src="/assets/icons/Empresas.svg" style={{height:'18px', width:'18px'}} />
                  </button>

                                                                                      {/*     316px*/ }
        <div  style={{position:'absolute', top:'0px', width:'219px', marginTop:'56px', height:'316px',marginLeft:'77px',maxHeight:'calc(-57px + 100vh)',maxWidth: 'calc(-8px + 100vw)',visibility:verPerfil?'hidden':'visible', transition:'height 4s linear 0s' }} >
          <Perfil  onClickOutside={()=>{  setVerPerfil(false) }}    visible={verPerfil} />
        </div>


      </div>


    </header>
  );
}
